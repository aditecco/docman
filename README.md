DocMan
======

![](./.preview/dcm-01.png)

DocMan is your personal documentation manager. Write, generate & organize your documentation.

#### Stack

- Gatsby
- React
- TypeScript
